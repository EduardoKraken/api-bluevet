
const Clientes = require("../models/clientes.model.js");

exports.getClientes = (req,res) => {
  Clientes.getClientes((err, data) => {
    if (err)
      res.status(500).send({ message: err.message || "Se produjo algún error al recuperar los clientes" });
    else res.send(data);
  });
};


// Crear un cliente
exports.addCliente = (req, res) => {
  // Validacion de request
  if(!req.body){
    return res.status(400).send({ message:"El Contenido no puede estar vacio" });
  }

  // Guardar el CLiente en la BD
  Clientes.addCliente(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({ message: err.message || "Se produjo algún error al crear el cliente" })
    else res.status(200).send({ message:'El cliente se creo correctamente'})
  })
};

exports.updateCliente = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
  Clientes.updateCliente(req.body ,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No encontre el cliente con el id ${req.params.id }.`});
      } else {
        res.status(500).send({ message: "Error al actualizar el cliente con el id" + req.params.id });
      }
    } 
    else res.status(200).send({ message: 'La información se actualizo correctamente.'});
  });
}


exports.getMascotas = (req,res) => {
  Clientes.getMascotas((err, data) => {
    if (err)
      res.status(500).send({ message: err.message || "Se produjo algún error al recuperar los clientes" });
    else res.send(data);
  });
};


// Crear un cliente
exports.addMascota = (req, res) => {
  // Validacion de request
  if(!req.body){
    return res.status(400).send({ message:"El Contenido no puede estar vacio" });
  }

  // Guardar el CLiente en la BD
  Clientes.addMascota(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({ message: err.message || "Se produjo algún error al crear el cliente" })
    else res.status(200).send({ message:'El cliente se creo correctamente'})
  })
};

exports.updateMascota = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
  Clientes.updateMascota(req.body ,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No encontre el cliente con el id ${req.params.id }.`});
      } else {
        res.status(500).send({ message: "Error al actualizar el cliente con el id" + req.params.id });
      }
    } 
    else res.status(200).send({ message: 'La información se actualizo correctamente.'});
  });
}








