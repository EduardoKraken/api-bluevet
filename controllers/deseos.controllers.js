
const Deseos = require("../models/deseos.model.js");

exports.anadir_lista_deseos = (req, res)=>{
  Deseos.buscar_producto_lista_deseos(req.body, (err, data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún erro al recuperar las ciudades."
      });
    }else{
      // console.log('data', data);
      if(!data.length){
        Deseos.anadir_lista_deseos(req.body, (err2, data2)=>{
          if(err2){
            res.status(500).send({
              message:
              err2.message || "Se produjo algún error al añadir a la lista de deseos"
            })
          }else{
            res.status(200).send({ message:'Se añadio a tú lista de deseos.'})
          }
        });
      }else{
        res.status(500).send({ message:'Este producto ya se encuentra en tú lista actual'})
      }
    }
  });
};

exports.obtener_lista_deseos = (req, res)=>{
  Deseos.obtener_lista_deseos(req.params.id_cliente, (err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else {
      res.status(200).send(data);
    }
      
  });
};

exports.eliminar_producto_lista_deseos = (req, res) => {
  Deseos.eliminar_producto_lista_deseos(req.params.idlista, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el producto en la lista con id  ${req.params.idlista}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el producto en la lista con el id " + req.params.idlista
        });
      }
    } else res.status(200).send({ message: `El producto se elimino de la lista!` });
  });
};








