const catalogos = require("../models/catalogos.model.js");

exports.getEspecies = (req, res)=>{
  catalogos.getEspecies((err,data)=>{
		if(err)
			res.status(500).send({ message: err.message || "Se produjo algún erro al recuperar los grupos por escuelas." });
		else res.send(data);
  });
};

exports.getEspeciesActivas = (req,res) => {
  catalogos.getEspeciesActivas((err, data) => {
    if (err)
      res.status(500).send({ message: err.message || "Se produjo algún error al recuperar los clientes"});
    else res.send(data);
  });
};

// Crear un cliente
exports.addEspecie = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({message:"El Contenido no puede estar vacio"});
  }

  // Guardar el CLiente en la BD
  catalogos.addEspecie(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({ message: err.message || "Se produjo algún error al crear el cliente"})
  	else res.status(200).send({ message:'El cliente se creo correctamente'})
  })
};

exports.updateEspecie = (req, res) =>{
	if (!req.body) {
		res.status(400).send({ message: "El Contenido no puede estar vacio!" });
	}
	
	catalogos.updateEspecie(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({ message: `No encontre el cliente con el id ${req.body.idtipos_mascota }.`});
			} else {
				res.status(500).send({ message: "Error al actualizar el cliente con el id" + req.body.idtipos_mascota });
			}
		} 
		else res.status(200).send({ message: 'La información se actualizo correctamente.'});
	});
}


exports.getRazas = (req, res)=>{
  catalogos.getRazas((err,data)=>{
		if(err)
			res.status(500).send({ message: err.message || "Se produjo algún erro al recuperar los grupos por escuelas." });
		else res.send(data);
  });
};

exports.getRazasActivas = (req,res) => {
  catalogos.getRazasActivas((err, data) => {
    if (err)
      res.status(500).send({ message: err.message || "Se produjo algún error al recuperar los clientes"});
    else res.send(data);
  });
};

// Crear un cliente
exports.addRaza = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({message:"El Contenido no puede estar vacio"});
  }

  // Guardar el CLiente en la BD
  catalogos.addRaza(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({ message: err.message || "Se produjo algún error al crear el cliente"})
  	else res.status(200).send({ message:'El cliente se creo correctamente'})
  })
};

exports.updateRaza = (req, res) =>{
	if (!req.body) {
		res.status(400).send({ message: "El Contenido no puede estar vacio!" });
	}
	
	catalogos.updateRaza(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({ message: `No encontre el cliente con el id ${req.body.idtipos_mascota }.`});
			} else {
				res.status(500).send({ message: "Error al actualizar el cliente con el id" + req.body.idtipos_mascota });
			}
		} 
		else res.status(200).send({ message: 'La información se actualizo correctamente.'});
	});
}