const existencias = require("../models/existencias.model.js");

exports.getExistencias =  async (req,res)=>{

  try{
    const activarGroupBy = await existencias.activarGroupBy( ).then( response => response)
    const getExistencias = await existencias.getExistencias( ).then( response => response)

    res.send(getExistencias)
    
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor, comunicate con sistemas '})
  }
};


// agregar una entrada
exports.getExistenciaCodigo = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  existencias.getExistenciaCodigo(req.params.codigo, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

// agregar una entrada
exports.getExistenciaCodigoDes = (req, res) => {
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  existencias.getExistenciaCodigoDes(req.params.codigo, (err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cliente"
      })
    else res.send(data)
  })
};


// // traer las entradas por fecha
exports.getExistenciaTotal = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  existencias.getExistenciaTotal((err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

