const entradas = require("../models/entradas.model.js");
const almacen = require("../models/almacen.model.js");

// traer las entradas por fecha
exports.getEntradasFecha = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  entradas.getEntradasFecha(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};


exports.addEntradas = async (req, res) => {
  try{
    // Agregamos la entrada
    const addEntradas  = await entradas
      .addEntradas(req.body)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    
    // Obtenemos el almacen
    const getAlmacen  = await almacen
      .getAlmacen(req.body)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Verificamos si existe en almacen el producto
    if(getAlmacen.length > 0){
      // Si existe se actualiza
      const updateAlmacen2 = await almacen
        .updateAlmacen2(req.body)
        .then((response) => response)
        .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    }else{
      // No existe, se agrega
      const addAlmacen = await almacen
        .addAlmacen(req.body)
        .then((response) => response)
        .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    }

    res.send({message: 'se agrego correctamente'});
  }catch(err){
    res.status(500).send({
      message:
      err.message || "Se produjo algún error al crea el pago"
    })
  }
};


exports.getEntradasAll =  (req,res)=>{
  entradas.getEntradasAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	res.send(data)
    } 
  });
};

exports.updateEntrada = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 entradas.updateEntrada(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 })
};



exports.eliminarEntrada = async (req, res) => {
  try{
    const { identradas, idsucursales, idarts } = req.body
    // Consutar primero si existe realmente en almace
    const articuloAlmacen = await almacen.getArticuloAlmacen( idarts, idsucursales ).then( response => response )

    // validamos si existe en almacen
    if ( !articuloAlmacen )
      return res.status(500).send({ message: "El artículo que intentas eliminar no esta en almacén" });

    const { idalmacen, cant } = articuloAlmacen

    // Hay que validar las cantidades para ver si si se puede eliminar
    const cantidadEliminar = req.body.cant
    if( cantidadEliminar > cant )
      return res.status(500).send({ message: "Cantidad insuficiente en almacén" });

    // ya validamos todo, lo podemos eliminar
    // ACtualizar el alacen
    const cantidadRestante = cant - cantidadEliminar
    const updateAlmacen = await almacen.eliminarAlmacen( idalmacen, cantidadRestante ).then( response => response )
    // Eliminar del almacen
    const eliminarEntrada = await entradas.eliminarEntrada( identradas ).then( response => response )

    console.log( { message: 'Datos eliminados correctamente' } )
    res.send(req.body);
  }catch(err){
    res.status(500).send({
      message:
      err.message || "Se produjo algún error al crea el pago"
    })
  }
};





