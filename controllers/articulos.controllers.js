const Articulos = require("../models/articulos.model.js");
// const Fotos = require("../models/articulos.model.js");
var articulos = []

exports.obtener_articulos_random = (req, res)=>{
  Articulos.obtener_articulos_random((err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else {
      res.status(200).send(data);
    }
      
  });
};

// Crear un cliente
exports.addArticulos = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Articulos.addArticulos(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.findAll =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else res.send(data);
  });
};

exports.getArticuloId =  (req,res)=>{
  Articulos.getArticuloId(req.params.id,(err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else res.send(data);
  });
};

exports.getArticulosMovil =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else{
      res.send(data)
    } 
  });
};

fotos = (callback) =>{
	Articulos.fotosArt((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	callback(data);
    }
  });
};

exports.getArticuloCodigo = (req, res)=>{
    Articulos.getArticuloCodigo(req.params.codigo,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo un error."
				});
      else res.send(data);
				
    });
};

exports.getArticuloTienda = (req, res)=>{
    Articulos.getArticuloTienda(req.params.codigo,(err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        else res.send(data);
    });
};


exports.updateArticulos = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateArticulos(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el articulo con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};

// Traer fotos por articulo
exports.fotosxArt = (req, res)=>{
  Articulos.fotosxArt(req.params.codigo,(err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      else res.send(data);
  });
};

// Agregar fotos por codigo
exports.addFoto = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
    Articulos.addFoto(req.body, (err, data)=>{
      // EVALUO QUE NO EXISTA UN ERROR
      if(err)
        res.status(500).send({
          message:
          err.message || "Se produjo algún error al crear el cliente"
        })
      else res.send(data)
    // })
  })
};

exports.updateNovedades = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateNovedades(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el articulo con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};

exports.getArticulosActivo = (req, res)=>{
  Articulos.getArticulosActivo((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      else res.send(data);
  });
};

// Obtener los artículos activos, primeeeero activos
exports.getArticulosCaja = async(req, res) => {
  try {
    const { id } = req.params
    let productos = await Articulos.getArticulosCaja( ).then( response => response )
    const habilitarGroup = await Articulos.habilitarGroup( ).then( response => response )

    const existencias = await Articulos.getExistencias( ).then( response=> response )
    // const sucursales  = await Articulos.getSucursales( ).then( response=> response )

    for( const i in productos ){
      const { id } = productos[i]
      // Buscamos en las sucursales
      const existeExistencia       = existencias.find( el => el.idarts == id )
      productos[i]['existencia']   = existeExistencia ? existeExistencia.suma : 0
    }

    res.send(productos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Delete a users with the specified usersId in the request
exports.deleteFoto = (req, res) => {
  Articulos.deleteFoto(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};


/*******************************************************************************/
exports.updateFotoPrincipal = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Articulos.updateFotoPrincipal(req.params.id, req.body ,(err, data) => {
  if (err) {
    if (err.kind === "not_found") {
      res.status(404).send({
        message: `No encontre el articulo con el id ${req.params.id }.`
      });
    } else {
      res.status(500).send({
        message: "Error al actualizar el articulo con el id" + req.params.id 
      });
    }
  } 
  else res.send(data);
 });
};

