const caja = require("../../models/caja/caja.model.js");


// llamara todos los Categorias excepto los deleted = 1
exports.exsiteCaja = (req, res) => {
  caja.exsiteCaja(req.params.id, (err, data) => {
		if (err){
			res.status(500).send({
				message: err.message || "Se produjo algún error al recuperar las categorias"
			});
		}

		if (data){
			res.send({ data });
		}else{
			res.status(400).send({ message:  "No hay caja abierta" });
		}
  });
};


exports.abrirCaja = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
	res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  caja.abrirCaja(req.body, (err, data) => {
	if (err)
	  res.status(500).send({
		message: err.message || "Se produjo algún error al crear la caja"
	  })
	else res.status(200).send(data);
  })
};


exports.addPagoCaja = async(req, res) => {
  try {
  	const { productos } = req.body
    const data = await caja.addPagoCaja( req.body ).then( response => response )
    const folio = `TC${data.id}` 
    const updataVenta = await caja.updateFolio( data.id, folio).then(response => response )

    // Necesitamos ingresar todo el detalle de la venta
    for(const i in productos){
    	const { id, cantidad, precio1, nomart, idcategorias, foto, iddoctores, completa, compra } = productos[i]
    	const payload = {
    		idventas      : data.id,
				idproductos		: id,
				cantidad			: cantidad,
				descuento			: 0,
				precio 				: precio1,
				total 				: cantidad * precio1,
				producto  		: nomart,
				foto 					: foto,
				idpromociones	: 0,
				iddoctores    : iddoctores ? iddoctores : 0,
				completa      : completa   ? completa   : 0,
        compra        : compra
    	}
    	const addDetalleVenta = await caja.addDetalleVenta( payload ).then(response => response )
    	const addSalida = await caja.addSalida( id, cantidad ).then(response => response )

    	// Descontar del almacen
    	console.log( id, cantidad )
    	const updateAlmacen   = await caja.updateAlmacen( id, cantidad ).then( response => response )
    }

		res.send(data);
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};


exports.getDetallesVenta = async(req, res) => {
  try {
  	const ventas   = await caja.getVentas().then(response => response)
  	const detalles = await caja.getDetallesVenta().then(response => response)

  	for(const i in ventas){
  		const { idventas } = ventas[i]
  		const detalles_venta = detalles.filter(el => { return el.idventas == idventas })

  		ventas[i] = {
  			...ventas[i],
  			detalles_venta
  		}
  	}


		res.send(ventas);
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};




exports.getEfectivoEnCaja = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar el efectivo en caja
  	const efectivoEnCaja = await caja.getEfectivoEnCaja( id ).then(response => response)
  	const getTotalRetiro = await caja.getTotalRetiro( id ).then(response => response)

  	let pago_efectivo  = 0
  	let cambio         = 0
  	let retiro         = 0

  	if(efectivoEnCaja){
  		pago_efectivo   = efectivoEnCaja.pago_efectivo
			cambio          = efectivoEnCaja.cambio
  	}

  	if(getTotalRetiro){
  		retiro = getTotalRetiro.retiro
  	}
  	const respuesta = {
			efectivo: (pago_efectivo - cambio ) - retiro  		
  	}

		res.send(respuesta);
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};

// Agregar un retiro de caja
exports.addRetiroCaja = async(req, res) => {
  try {
  	// Consultar el efectivo en caja
  	const addRetiro      = await caja.addRetiroCaja( req.body ).then(response => response)


		res.send(addRetiro);
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};

exports.addCierreCaja = async(req, res) => {
  try {
  	const { dinero_caja, diferencia, total_caja, idcaja } = req.body
  	// Consultar el efectivo en caja
  	const addCierreCaja      = await caja.addCierreCaja( dinero_caja, diferencia, total_caja, idcaja ).then(response => response)

		res.send(addCierreCaja);
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};



exports.getCajasCerradas = async(req, res) => {
  try {
  	const { fecha } = req.body
  	// Consultar el efectivo en caja
  	const  cajasCerradas       = await caja.getCajasCerradas( fecha ).then(response => response)

		res.send( cajasCerradas );
  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }
};


exports.getCajaCerrada = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar el efectivo en caja
  	let  cajaCerrada       = await caja.getCajaCerrada( id ).then(response => response)
  	let  ventas            = await caja.ventas( id ).then(response => response)

  	let idDetalles = ventas.map((registro)=>{ return registro.idventas })

  	let detalles = await caja.getDetalleVenta( idDetalles ).then(response => response)

  	for(const i in ventas){
  		const { idventas } = ventas[i]
  		const detalles_venta = detalles.filter(el => { return el.idventas == idventas })

  		ventas[i]['detalles'] = detalles_venta
  	}

  	let respuesta = {
  		caja:   cajaCerrada,
  		ventas: ventas
  	}

		res.send( respuesta );
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.getCierreCaja = async(req, res) => {
  try {
  	const { id } = req.params

  	const infoCaja = await caja.getInfoCaja( id ).then( response => response )


  	// Consultar el efectivo en caja
  	let  cajaCerrada       = await caja.getCajaCerrada( id ).then(response => response)
  	let  ventas            = await caja.ventas( id ).then(response => response)

  	let efectivo = ventas.map(item => item.pago_efectivo ).reduce((prev, curr) => prev + curr, 0);
  	let cambio   = ventas.map(item => item.cambio ).reduce((prev, curr) => prev + curr, 0);
  	let tarjeta  = ventas.map(item => item.pago_tarjeta ).reduce((prev, curr) => prev + curr, 0);

  	let respuesta = {
  		nombre:       infoCaja.nomper,
  		inicio:       infoCaja.fecha_creacion,
  		final:        infoCaja.fecha_cierre,
  		diferencia:   infoCaja.diferencia,
  		total_ventas: ventas.length,
  		fondoinicial: 750,
  		saldoencaja:  (efectivo - cambio) + 750,
  		efectivo:     efectivo - cambio,
  		tarjeta,
  		total:        tarjeta + (efectivo - cambio),
  	}

		res.send( respuesta );
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.getVentasDiarias = async(req, res) => {
  try {
  	const { fecha } = req.body

  	let  ventas  = await caja.getVentasDiarias( fecha ).then(response => response)

		res.send( ventas );
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};
