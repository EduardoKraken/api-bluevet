const productos = require("../models/articulos.model.js");
const { v4: uuidv4 } = require('uuid');


// Guardar imagenes
exports.addImagen = (req, res)=>{
  let EDFile = req.files.file
  EDFile.mv(`./../../fotos-fetish/${EDFile.name}`,err => {
      if(err) return res.status(500).send({ message : err })

      return res.status(200).send({ message : 'File upload' })
  })
};


// Guardar PDF
exports.addPdf = (req, res)=>{
  let EDFile = req.files.file
  EDFile.mv(`./../../pdf_bluevet/${EDFile.name}`,err => {
    if(err) return res.status(500).send({ message : err })
    return res.status(200).send({ message : 'File upload' })
  })
};

exports.getDocumento = (req, res) =>{
  var file ='./../../pdf/' + req.params.nom ;
  res.download(file); // Set disposition and send it.
}

exports.addImagenes = async(req, res) => {
  if(!req.files){
    return res.status(400).send({message: 'Favor de cargar una imagen'})
  }

  // recuperamos el archivos
  const { file } = req.files //EDFile.name
  // separamos todo el nombre en partes, separado por .
  const nombreCortado = file.name.split('.')
  // Sacamos la extension
  const extension = nombreCortado[ nombreCortado.length - 1 ]
  // extensiones validas
  const extensiones = [ 'png', 'jpg', 'jpeg', 'gif' ] 
  // Validamos la extensión
  if( !extensiones.includes( extension ) ){
    // Mostramos el error de que no es valida esa extnsión
    return res.status(400).send({message: `La extensión: ${ extension } no es permitida, ${ extensiones }`})
  }
  // Creamos un nombre aca encripatado bien chido 
  const nombreTemp  = uuidv4() + '.' + extension
  // Creamos la ruta
  const uploadPath = `./../../imagenes-bluevet/${nombreTemp}`

  // Primero guardamos la foto en la base de datos y a su productos
  const { id } = req.params
  
  // Agregar la imagen al servidor
  await addImagenServidor( uploadPath, file ).then(response => response)
  
  // Lo gradamos
  const addImagenProducto = await productos.addImagenProducto( nombreTemp, id ).then( response=> response )

  res.send({ message : 'Archivo Cargado', uploadPath })
};

addImagenServidor = ( uploadPath, file )=> {
  return new Promise((resolve,reject)=>{
    file.mv(uploadPath,  (err) => {
      if (err){
        return reject( err )
      } 
      resolve({ message: 'File upload' })
    })
  })
}
