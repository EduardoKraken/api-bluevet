const Ordenes = require("../models/ordenes.model.js");

// agregar una entrada
exports.addOrden = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Ordenes.addOrden(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la orden"
  		})
  	else res.send(data)
  })
};


// traer las entradas por fecha
exports.getOrdenesUsuario = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Ordenes.getOrdenesUsuario(req.params.id, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getOrdenes = async (req, res) => {
  try{
    // Paso #1 Obtener los datos de docum
    const pedidos = await Ordenes.getDocumen()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    let pedidosRespuesta = []

    // Paso 2-. Obtener de cada uno de ellos los movimientos
    const movimientos = await Ordenes.getMovimientos()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #3-. construir el arreglo de movimientos

    for(const i in pedidos){
      let docum = {
        ...pedidos[i],
        movim:                  [],
        direcciones:            null,
        facturacion:            null,
        cliente:                null,
        pagado:                 null
      }

      pedidosRespuesta.push(docum)
    }

    // Paso #5 consultamos las direcciones
    const direcciones = await Ordenes.getDirecciones()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #6 consultamos las direcciones de facturación
    const direccionesFac = await Ordenes.getDireccionesFac()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #7 consultamos al cliente
    const clientes = await Ordenes.getClientesOrden()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #8 cargar los pagos
    const pagos_mercadopago = await Ordenes.getPagosMercaddoPago()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));


    for(const i in pedidosRespuesta){
      const movimiento = movimientos.filter((movim)=>{
                          var doc = movim.iddocum;
                          return doc == pedidosRespuesta[i].iddocum;
                        })
      pedidosRespuesta[i].movim = movimiento
    }

    // Agregar la direccion
    for(const i in pedidosRespuesta){
      const direccion = direcciones.filter(dir=> dir.idpago == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].direcciones = direccion
    }

    // Agregar la factura
    for(const i in pedidosRespuesta){
      const factura = direccionesFac.filter(dir=> dir.idpago == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].facturacion = factura
    }

    // Agregar al cliente
    for(const i in pedidosRespuesta){
      const cliente = clientes.filter(cli=> cli.idcliente == pedidosRespuesta[i].idcliente)
      pedidosRespuesta[i].cliente = cliente
    }

    // Agregar al cliente
    for(const i in pedidosRespuesta){
      const pago = pagos_mercadopago.filter(pago=> pago.preference_id == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].pagado = pago
    }

    let resultado = []

    for(const i in pedidosRespuesta){
      if(pedidosRespuesta[i].pagado.length > 0){
        resultado.push(pedidosRespuesta[i])
      }
    }

    res.send(resultado);
  }catch(e){
    res.status(500).send({message: e || "Se produjo algún error al crea el pago"})
  }
};


exports.getOrdenesCliente = async (req, res) => {
  try{
    // Paso #1 Obtener los datos de docum
    const pedidos = await Ordenes.getDocumenCli(req.params.idcliente)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    let pedidosRespuesta = []

    // Paso 2-. Obtener de cada uno de ellos los movimientos
    const movimientos = await Ordenes.getMovimientos()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));


    // Paso #3-. construir el arreglo de movimientos

    for(const i in pedidos){
      let docum = {
        iddocum:                pedidos[i].iddocum,
        idcliente:              pedidos[i].idcliente,
        iddireccionfacturacion: pedidos[i].iddireccionfacturacion,
        iddirecion:             pedidos[i].iddirecion,
        idpago:                 pedidos[i].idpago,
        descuento:              pedidos[i].descuento,
        subtotal:               pedidos[i].subtotal,
        total:                  pedidos[i].total,
        idcupon:                pedidos[i].idcupon,
        envio:                  pedidos[i].envio,
        envio_gratis:           pedidos[i].envio_gratis,
        descuento_cupon:        pedidos[i].descuento_cupon,
        movim:                  [],
        direcciones:            null,
        facturacion:            null,
        cliente:                null,
        rastreo:                null
      }

      pedidosRespuesta.push(docum)
    }

    // Paso #5 consultamos las direcciones
    const direcciones = await Ordenes.getDirecciones()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #6 consultamos las direcciones de facturación
    const direccionesFac = await Ordenes.getDireccionesFac()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #7 consultamos al cliente
    const clientes = await Ordenes.getClientesOrden()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #8 consultamos los rastreos
    const rastreos = await Ordenes.getRastreoCliente()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

      // Paso #8 cargar los pagos
    const pagos_mercadopago = await Ordenes.getPagosMercaddoPago()
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));


    for(const i in pedidosRespuesta){
      const movimiento = movimientos.filter((movim)=>{
                          var doc = movim.iddocum;
                          return doc == pedidosRespuesta[i].iddocum;
                        })
      pedidosRespuesta[i].movim = movimiento
    }

    // Agregar la direccion
    for(const i in pedidosRespuesta){
      const direccion = direcciones.filter(dir=> dir.idpago == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].direcciones = direccion
    }

    // Agregar la factura
    for(const i in pedidosRespuesta){
      const factura = direccionesFac.filter(dir=> dir.idpago == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].facturacion = factura
    }

    // Agregar al cliente
    for(const i in pedidosRespuesta){
      const cliente = clientes.filter(cli=> cli.idcliente == pedidosRespuesta[i].idcliente)
      pedidosRespuesta[i].cliente = cliente
    }

    // Agregar el rastreo
    for(const i in pedidosRespuesta){
      const rastreo = rastreos.filter(ras=> ras.idpago == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].rastreo = rastreo
    }

    // Agregar al cliente
    for(const i in pedidosRespuesta){
      const pago = pagos_mercadopago.filter(pago=> pago.preference_id == pedidosRespuesta[i].idpago)
      pedidosRespuesta[i].pagado = pago
    }

    let resultado = []

    for(const i in pedidosRespuesta){
      if(pedidosRespuesta[i].pagado.length > 0){
        resultado.push(pedidosRespuesta[i])
      }
    }

    res.send(resultado);
  }catch(e){
    res.status(500).send({message: e || "Se produjo algún error al crea el pago"})
  }
};

exports.getOrdenId =  (req,res)=>{
  Ordenes.getOrdenId(req.params.id,(err, docum) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las ordenes"
      });
    }else{
    	Ordenes.getMovim(req.params.id,(err, movim) => {
		    if (err){
		      res.status(500).send({
		        message:
		          err.message || "Se produjo algún error al recuperar las ordenes"
		      });
		    }else{
		    	console.log('docum',docum)
		    	var respuesta = {
					  iddocum         :docum[0].iddocum,
					  idusuariosweb   :docum[0].idusuariosweb,
					  direccion       :docum[0].direccion,
					  importe         :docum[0].importe,
					  descuento       :docum[0].descuento,
					  subtotal        :docum[0].subtotal,
					  total           :docum[0].total,
					  iva             :docum[0].iva,
					  folio           :docum[0].folio,
					  estatus         :docum[0].estatus,
					  nota            :docum[0].nota,
					  divisa          :docum[0].divisa,
					  hora            :docum[0].hora,
					  fecha           :docum[0].fecha,
					  fechapago       :docum[0].fechapago,
					  refer           :docum[0].refer,
					  tipodoc         :docum[0].tipodoc,
					  movim           :movim
					}

					console.log('respuest',respuesta)

		    	res.send(respuesta)
		    }
		  })
    } 
  });
};

exports.updateOrden = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Ordenes.updateOrden(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};


exports.updateOrdenEnvio = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Ordenes.updateOrdenEnvio(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};






