const sql = require("./db.js");

// constructor

const entradas = entradas => {
  this.identradas     = entradas.identradas;
  this.nomart         = entradas.nomart;
  this.codigo         = entradas.codigo;
  this.fechaentr      = entradas.fechaentr;
  this.lote           = entradas.lote;
  this.cant           = entradas.cant;
  this.caducidad      = entradas.caducidad;
  this.proveedor      = entradas.proveedor;
  this.factura        = entradas.factura;
};

entradas.addEntradas = (a, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO entradas (idarts, cant, idsucursales ) VALUES(?,?,?)`,
      [a.idarts, a.cant, a.idsucursales], (err, res) => {  
      if (err) {
        reject(err)
        return;
      }
      resolve({ id: res.insertId, ...a })
    });
  })
};

entradas.getEntradasAll = result => {
  sql.query(`SELECT e.identradas, a.nomart, a.codigo, e.idarts, e.fecha_creo, e.cant, e.idsucursales, s.sucursal FROM entradas e 
    LEFT JOIN arts a ON e.idarts = a.id
    LEFT JOIN sucursales s ON s.idsucursales = e.idsucursales;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

entradas.getEntradasFecha = (fecha, result) => {
  sql.query(`SELECT e.identradas, a.nomart, e.idarts, a.codigo, e.fecha_creo, e.cant FROM  entradas e 
    LEFT JOIN arts a ON e.idarts = a.id
    LEFT JOIN sucursales s ON s.idsucursales = e.idsucursales
    WHERE DATE(e.fecha_creo) BETWEEN ? AND ?`,
    [fecha.inicio,fecha.fin], (err, res) => {  
    	console.log(sql)
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

entradas.updateEntrada = (id, ent, result) => {
  sql.query(` UPDATE entradas SET lote = ?, proveedor =?, factura =?, caducidad=?
                WHERE identradas = ?`, [ent.lote, ent.proveedor,ent.factura,ent.caducidad,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated enticulos: ", { id: id, ...ent });
      result(null, { id: id, ...ent });
    }
  );
};

entradas.eliminarEntrada = ( identradas ) => {
  return new Promise(( resolve, reject ) => {
    sql.query(`DELETE FROM entradas WHERE identradas = ? ;`,[ identradas ],(err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};

module.exports = entradas;