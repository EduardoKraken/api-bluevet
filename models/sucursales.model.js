const sql = require("./db.js");

// constructor

const Sucursales = almacen => {
  this.codigo      = almacen.codigo;
  this.lote        = almacen.lote;
  this.cant        = almacen.cant;
  this.caducidad   = almacen.caducidad;
};

/*************************************************************/
/*									E S P E C I E S                          */
/*************************************************************/

Sucursales.getSucursales = result => {
  sql.query(`SELECT * FROM sucursales;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Sucursales.addSucursales = (ent, result) => {
  sql.query(`INSERT INTO sucursales (sucursal,estatus)
                                  VALUES(?,?)`,
    [ent.sucursal, ent.estatus], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("Crear especie: ", { id: res.insertId, ...ent });
    result(null, { id: res.insertId, ...ent });
  });
};

Sucursales.updateSucursales = (id, alm, result) => {
  sql.query(`UPDATE sucursales SET sucursal = ?, estatus = ? WHERE idsucursales= ? `, [alm.sucursal, alm.estatus, id],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("updated especie: ", { id: id, ...alm });
    result(null, { id: id, ...alm });
  });
};

Sucursales.getSucursalesActivas = result => {
  sql.query(`SELECT * FROM sucursales WHERE estatus = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};



module.exports = Sucursales;