const sql = require("./db.js");

// constructor

const existencias = existencias => {
  this.idexistencias  = existencias.idexistencias;
  this.nomart         = existencias.nomart;
  this.codigo         = existencias.codigo;
  this.lote           = existencias.lote;
  this.cant           = existencias.cant;
  this.caducidad      = existencias.caducidad;
  this.suma           = existencias.suma;
  this.lab            = existencias.lab;
  this.sal            = existencias.sal;
};

existencias.activarGroupBy = result => {
  return new Promise(( resolve, reject ) => {
    sql.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`, (err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};


existencias.getExistencias = result => {
  return new Promise(( resolve, reject ) => {
    sql.query(`SELECT a.idalmacen, a.idarts, sum(a.cant) AS suma, ar.nomart, ar.id, ar.codigo, s.sucursal  FROM almacen a
      LEFT JOIN arts ar ON a.idarts = ar.id
      LEFT JOIN sucursales s ON s.idsucursales = a.idsucursales
      GROUP BY  ar.id, s.sucursal;`, (err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  });
};


existencias.getExistenciaCodigo =(codigo, result) => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like'%`+ codigo +`%' AND a.cant > 0
        ORDER BY a.caducidad ASC, a.cant DESC ;`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

existencias.getExistenciaCodigoDes =(codigo, result) => {
  sql.query(`SELECT a.lote, ar.nomart, l.nomlab AS "lab",a.caducidad, SUM(a.cant) AS cant, ar.sal
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio 
        WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like"%`+codigo+`%" AND a.cant > 0
        GROUP BY a.lote, ar.nomart, l.nomlab, ar.sal,a.caducidad;`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};


existencias.getExistenciaTotal = result => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
    FROM almacen  a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
    l.idlaboratorios = ar.idlaboratorio WHERE a.cant > 0 ORDER BY a.caducidad ASC, a.cant DESC;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};



module.exports = existencias;