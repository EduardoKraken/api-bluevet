const sql = require("./db.js");

// constructor
const catalogos = (catalogos) => { };


catalogos.getEspecies = (result)=>{
	sql.query(`SELECT * FROM tipos_mascota WHERE deleted = 0;`, (err,res)=>{
		if (err) {
      return result(null, err);
    }
    result(null, res);
	})
};

catalogos.getEspeciesActivas = result => {
  sql.query(`SELECT * FROM tipos_mascota WHERE deleted = 0;`, (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  });
};

catalogos.addEspecie = (c, result) => {
	sql.query(`INSERT INTO tipos_mascota( tipo )VALUES( ? )`, [ c.tipo ], 
    (err, res) => {	
    if (err) {
      return result(err, null);
    }
    result(null, { id: res.insertId, ...c });
	});
};

catalogos.updateEspecie = (cli, result) => {
  sql.query(` UPDATE tipos_mascota SET tipo = ?, deleted = ? WHERE idtipos_mascota = ?`, [cli.tipo, cli.deleted, cli.idtipos_mascota ],
    (err, res) => {
      if (err) {
        return result(null, err);
      }

      if (res.affectedRows == 0) {
        return result({ kind: "not_found" }, null);
      }
      result(null, true);
    }
  );
};

catalogos.getRazas = (result)=>{
	sql.query(`SELECT r.*, e.tipo FROM razas r 
		LEFT JOIN tipos_mascota e ON e.idtipos_mascota = r.idtipos_mascota
		WHERE r.deleted = 0;`, (err,res)=>{
		if (err) {
      return result(null, err);
    }
    result(null, res);
	})
};

catalogos.getRazasActivas = result => {
  sql.query(`SELECT * FROM razas WHERE deleted = 0;`, (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  });
};

catalogos.addRaza = (c, result) => {
	sql.query(`INSERT INTO razas( raza, idtipos_mascota )VALUES( ?, ? )`, [ c.raza, c.idtipos_mascota ], 
    (err, res) => {	
    if (err) {
      return result(err, null);
    }
    result(null, { id: res.insertId, ...c });
	});
};

catalogos.updateRaza = (cli, result) => {
  sql.query(` UPDATE razas SET raza = ?, idtipos_mascota = ?, deleted = ? WHERE idrazas = ?`,
  	[cli.raza, cli.idtipos_mascota, cli.deleted, cli.idrazas ],
    (err, res) => {
      if (err) {
        return result(null, err);
      }

      if (res.affectedRows == 0) {
        return result({ kind: "not_found" }, null);
      }
      result(null, true);
    }
  );
};

module.exports = catalogos;