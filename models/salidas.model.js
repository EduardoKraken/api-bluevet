const sql = require("./db.js");

// constructor

const Salidas = entradas => {
  this.idsalidas    = entradas.idsalidas;
  this.codigo       = entradas.codigo;
  this.caducidad    = entradas.caducidad;
  this.cant         = entradas.cant;
  this.lote         = entradas.lote;
  this.fecha        = entradas.fecha;
  this.recibido     = entradas.recibido;
  this.entrego      = entradas.entrego;
  this.nomart       = entradas.nomart;
  this.nomuser      = entradas.nomuser;
};


Salidas.addSalidas = (art, result) => {
  sql.query(`INSERT INTO salidas (codigo,caducidad,cant,lote,fecha,recibio,entrego)
                                  VALUES(?,?,?,?, ?,?,?)`,
    [art.codigo, art.caducidad,art.cant,art.lote,art.fecha,art.recibio,art.entrego], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...art });
    result(null, { id: res.insertId, ...art });
  });
};

Salidas.getSalidasAll = result => {
  sql.query(`SELECT s.idsalidas, s.codigo, s.cant, s.lote, s.fecha, s.recibio, a.nomart, u.nomuser, s.caducidad, s.cant 
        FROM salidas s INNER JOIN arts a ON s.codigo = a.codigo INNER JOIN usuariosweb u 
        ON u.idusuariosweb = s.entrego WHERE s.cant > 0;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Salidas.getSalidasFecha = (fecha, result) => {
  sql.query(`SELECT s.idsalidas, s.codigo, s.cant, s.caducidad, s.lote, s.fecha, s.recibio, a.nomart , u.nomuser
        FROM salidas s INNER JOIN arts a ON s.codigo = a.codigo INNER JOIN usuariosweb u 
        ON u.idusuariosweb = s.entrego WHERE s.cant > 0 AND fecha BETWEEN  ? AND ? 
        ORDER BY s.fecha ASC`,
    [fecha.inicio,fecha.fin], (err, res) => {  
    	console.log(sql)
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

module.exports = Salidas;