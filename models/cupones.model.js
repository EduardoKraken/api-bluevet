
const sql = require("./db.js");
const Cupones = cupon => new Object();



Cupones.validar_cupon_existente = (data, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM cupones WHERE cupon = ? AND CURDATE() BETWEEN fecha1 AND fecha2 AND estatus = 1`,[ data.cupon ], (err, res) => {
     if (err) {
       reject(err);
     }
     console.log('VALIDACION CUPON', res)
     resolve(res);
   });
 })
};

Cupones.addCupon = (c, result) => {
  sql.query(`INSERT INTO cupones( cupon, fecha1,fecha2,cantidad,canjeados,descuento,tipo, estatus)VALUES(?,?,?,?,?,?,?,?)`, [c.cupon, c.fecha1,c.fecha2,c.cantidad,c.canjeados,c.descuento,c.tipo, c.estatus], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear cupon: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

Cupones.getCupones = (result)=>{
  sql.query(`SELECT * FROM cupones`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};

Cupones.updateCupon = (id, c, result) => {
  sql.query(` UPDATE cupones SET cupon=?, fecha1=?,fecha2=?,cantidad=?,canjeados=?,descuento=?,tipo=?, estatus=? WHERE idcupones = ?`, 
    [c.cupon, c.fecha1,c.fecha2,c.cantidad,c.canjeados,c.descuento,c.tipo, c.estatus,id], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated cupon: ", { id: id, ...c });
      result(null, { id: id, ...c });
    }
  );
};

module.exports = Cupones;
