const sql = require("./db.js");

// constructor

const alamacen = almacen => {
  this.codigo      = almacen.codigo;
  this.lote        = almacen.lote;
  this.cant        = almacen.cant;
  this.caducidad   = almacen.caducidad;
};

alamacen.addAlmacen = (ent, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO almacen (idarts,cant,idsucursales)
                                  VALUES(?,?,?)`,
      [ent.idarts,ent.cant,ent.idsucursales], (err, res) => { 
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};

alamacen.getAlmacen = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM almacen WHERE idarts = ? AND idsucursales = ?`, [art.idarts, art.idsucursales],(err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};

alamacen.getArticuloAlmacen = ( idarts, idsucursales ) => {
  return new Promise(( resolve, reject ) => {
    sql.query(`SELECT * FROM almacen WHERE idarts = ? AND idsucursales = ?;`, [ idarts, idsucursales ],(err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res[0] )
    });
  });
};


alamacen.updateAlmacen2 = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE almacen SET cant = cant + ? WHERE idalmacen > 0 AND idarts = ? AND idsucursales` , 
      [art.cant, art.idarts, art.idsucursales],(err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};


alamacen.eliminarAlmacen = ( idalmacen, cantidadRestante ) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE almacen SET cant = ?  WHERE idalmacen = ?;` , 
      [cantidadRestante, idalmacen],(err, res) => {
      if (err) 
        return reject({ message: err.sqlMessage })
      
      resolve ( res )
    });
  })
};

// alamacen.addAlmacen = (ent, result) => {
//   sql.query(`INSERT INTO almacen (codigo,lote,cant,caducidad)
//                                   VALUES(?,?,?,?)`,
//     [ent.codigo,ent.lote,ent.cant,ent.caducidad], (err, res) => {  
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//       return;
//     }
//     // console.log("Crear Grupo: ", res.insertId);
//     console.log("Crear almacen: ", { id: res.insertId, ...ent });
//     result(null, { id: res.insertId, ...ent });
//   });
// };

alamacen.getAlmacenList = result => {
  sql.query(`SELECT a.*, ar.nomart FROM almacen a 
    LEFT JOIN arts ar ON ar.codigo = a.codigo;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

alamacen.updateAlmacen = (id, alm, result) => {
  sql.query(`UPDATE almacen SET cant = ? WHERE idalmacen = ? `, [alm.cant, id],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("updated almacen: ", { id: id, ...alm });
    result(null, { id: id, ...alm });
  });
};

alamacen.validarAlmacen = (ent, result) => {
  sql.query(`SELECT a.codigo, a.id, SUM(alm.cant) AS cant , a.nomart, 
            (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto 
    FROM almacen alm LEFT JOIN arts a ON a.codigo = alm.codigo WHERE a.id IN (?) GROUP BY a.codigo, a.id;`,
    [ent], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

module.exports = alamacen;