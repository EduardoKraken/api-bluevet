const sql = require("./db.js");

// constructor
const Clientes = function(clientes) {
  this.idcliente = clientes.idcliente;
  this.nombre    = clientes.nombre;
  this.password  = clientes.password;
  this.telefono  = clientes.telefono;
  this.estatus   = clientes.estatus;
};

Clientes.getClientes = result => {
  sql.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', apellido_materno ) AS nombre_completo FROM clientes WHERE deleted = 0;`, (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  });
};

Clientes.addCliente = (c, result) => {
  sql.query(`INSERT INTO clientes(nombre,apellido_paterno,apellido_materno,movil,telefono,correo,direccion)VALUES(?,?,?,?,?,?,?)`,
    [c.nombre,c.apellido_paterno,c.apellido_materno,c.movil,c.telefono,c.correo,c.direccion], 
    (err, res) => { 
    if (err) {
      return result(err, null);
    }
    result(null, { id: res.insertId, ...c });
  });
};


Clientes.updateCliente = (cli, result) => {
  sql.query(` UPDATE clientes SET nombre=?,apellido_paterno=?,apellido_materno=?,movil=?,telefono=?,correo=?,direccion=?,deleted=?
    WHERE idclientes = ?`, 
    [cli.nombre,cli.apellido_paterno,cli.apellido_materno,cli.movil,cli.telefono,cli.correo,cli.direccion,cli.deleted,cli.idclientes],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { ...cli.data });
    }
  );
};


Clientes.getMascotas = result => {
  sql.query(`SELECT m.*, e.tipo AS especie, r.raza, CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno ) AS cliente  FROM mascotas m
    LEFT JOIN tipos_mascota e ON e.idtipos_mascota = m.idtipos_mascota
    LEFT JOIN razas r ON r.idrazas = m.idrazas
    LEFT JOIN clientes c ON c.idclientes = m.idclientes
    WHERE m.deleted = 0;`, (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  });
};

Clientes.addMascota = (c, result) => {
  sql.query(`INSERT INTO mascotas(mascota,sexo,edad,idtipos_mascota,idrazas,idclientes,tamanio,peso_mascota,observaciones)VALUES(?,?,?,?,?,?,?,?,?)`,
    [c.mascota,c.sexo,c.edad,c.idtipos_mascota,c.idrazas,c.idclientes,c.tamanio,c.peso_mascota,c.observaciones], 
    (err, res) => { 
    if (err) {
      return result(err, null);
    }
    result(null, { id: res.insertId, ...c });
  });
};


Clientes.updateMascota = (cli, result) => {
  sql.query(` UPDATE mascotas SET mascota=?,sexo=?,edad=?,idtipos_mascota=?,idrazas=?,idclientes=?,tamanio=?,deleted=?,peso_mascota=?,observaciones=?
    WHERE idmascotas = ?`, 
    [cli.mascota,cli.sexo,cli.edad,cli.idtipos_mascota,cli.idrazas,cli.idclientes,cli.tamanio,cli.deleted,cli.peso_mascota,cli.observaciones,cli.idmascotas],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { ...cli.data });
    }
  );
};

module.exports = Clientes;