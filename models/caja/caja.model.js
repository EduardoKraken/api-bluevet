const sql = require("../db.js");

//const constructor
const caja = function(operadores) {};

caja.exsiteCaja = ( idusuarios, result ) => {
  sql.query(`SELECT * FROM caja WHERE idusuarios = ? AND estatus = 1 AND deleted  = 0 ;`,[ idusuarios ], (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    result(null, res[0]);
  });
};

caja.abrirCaja = (u, result) => {
  sql.query(`INSERT INTO caja (idusuarios, datacaja )VALUES(?,?)`, [u.idusuarios, u.datacaja ],(err, res) => {
    if (err) { 
      result(err, null)
      return
    }
    result(null, { id: res.insertId, ...u });
  })
}


caja.addPagoCaja = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO ventas ( tipo_pago, idusuario, pago_tarjeta, pago_efectivo, cambio, subtotal, descuento, total, idcaja, redondeo )VALUES(?,?,?,?,?,?,?,?,?,?);`,
      [c.tipo_pago, c.idusuario, c.pago_tarjeta, c.pago_efectivo, c.cambio, c.subtotal, c.descuento, c.total, c.idcaja, c.redondeo ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c})
    })
  })
}

caja.addSalida = ( idarts, cant ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO salidas ( idarts, cant )VALUES(?,?);`,
      [ idarts, cant ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, idarts, cant})
    })
  })
}

caja.updateAlmacen = ( id, cantidad ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE almacen SET cant = ( cant - ? )  WHERE idalmacen > 0 AND idarts = ?;`,
      [ cantidad, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id, cantidad })
    })
  })
}

caja.updateFolio = ( id, folio ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE ventas SET folio = ? WHERE idventas = ? ;`,[ folio, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id, folio })
    })
  })
}

caja.addDetalleVenta = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO detalle_venta ( idventas, idproductos, cantidad, descuento, precio, total, producto, foto, idpromociones, compra )
      VALUES(?,?,?,?,?,?,?,?,?,?);`,
      [c.idventas, c.idproductos, c.cantidad, c.descuento, c.precio, c.total, c.producto, c.foto, c.idpromociones, c.compra ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c})
    })
  })
}

caja.getVentas = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM ventas;`, (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}

caja.getDetallesVenta = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM detalle_venta;`, (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}



caja.getEfectivoEnCaja = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT SUM(pago_efectivo) AS pago_efectivo, SUM(cambio) AS cambio  FROM ventas 
      WHERE idcaja = ? AND estatus = 1 AND deleted = 0 AND tipo_pago IN (2,3)
      GROUP BY idcaja;`,[ id ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

// Agregar un retiro de caja

caja.addRetiroCaja = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO retiros ( idcaja, retiro )VALUES(?,?);`,
      [ c.idcaja, c.retiro ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c})
    })
  })
}

// Obtener el total de dinero retirado de la caja
caja.addCierreCaja = ( dinero_caja, diferencia, total_caja, idcaja ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE caja SET estatus = 0, fecha_cierre = NOW(), dinero_caja = ?, diferencia = ?, total_caja = ? WHERE idcaja = ? ;`,
      [ dinero_caja, diferencia, total_caja, idcaja ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ dinero_caja, diferencia, total_caja, idcaja })
    })
  })
}

// Obtener el total de dinero retirado de la caja
caja.getTotalRetiro = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT SUM(retiro) AS retiro FROM retiros WHERE idcaja = ? AND deleted = 0 GROUP BY idcaja;`,[ id ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}


caja.getCajasCerradas = ( fecha ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT u.nomper, c.idcaja, c.fecha_creacion, c.fecha_cierre, c.dinero_caja, c.total_caja, c.diferencia FROM caja c
        LEFT JOIN usuariosweb u ON u.idusuariosweb = c.idusuarios
        WHERE DATE(c.fecha_creacion) = ?;`,[ fecha ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    })
  })
}

caja.getCajaCerrada = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT u.nomper, c.idcaja, c.fecha_creacion, c.fecha_cierre, c.dinero_caja, c.total_caja, c.diferencia FROM caja c
        LEFT JOIN usuariosweb u ON u.idusuariosweb = c.idusuarios
        WHERE c.idcaja = ?;`,[ id ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    })
  })
}

caja.ventas = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM ventas WHERE idcaja = ? AND deleted = 0;`,[ id ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    })
  })
}


caja.getDetalleVenta = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM detalle_venta WHERE idventas IN ( ? );`,[ id ], (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    })
  })
}


caja.getInfoCaja = ( idcaja ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT c.idcaja, c.fecha_creacion, c.fecha_cierre, c.dinero_caja, c.total_caja, c.diferencia, u.nomper FROM caja c 
      LEFT JOIN usuariosweb u ON u.idusuariosweb = c.idusuarios
      WHERE c.idcaja = ?;`,[ idcaja ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}


caja.getVentasDiarias = ( fecha ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT *, (precio - compra ) * cantidad AS ganancia FROM detalle_venta WHERE DATE(fecha_creacion) = ?;`,[ fecha ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    })
  })
}

module.exports = caja;