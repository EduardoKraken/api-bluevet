const sql = require("./db.js");

const Direcciones = function(direccion) {
    this.iddireccion  = direccion.iddireccion;
};

Direcciones.direcciones_cliente = (id, result)=>{
	sql.query(`SELECT d.* FROM direcciones d WHERE idcliente=? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direcciones: ", res);
    result(null, res);
	})
};
Direcciones.agregar_direccion_cliente = (d, result) => {
  console.log('d',d)
	sql.query(`INSERT INTO direcciones(idcliente,colonia,nombre,apellido,estado,municipio,calle,numero,cp,email,telefono, activo)
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,[d.idcliente ,d.data.colonia ,d.data.nombre,d.data.apellido, d.data.estado, d.data.municipio,
                                    d.data.calle,d.data.numero,d.data.cp,d.data.email,d.data.telefono, d.activo], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    console.log("Crear Direcciones: ", { id: res.insertId, ...d });
    result(null, { id: res.insertId, ...d });
	});
};
Direcciones.actualiza_direccion_envio = (d, result) => {
  console.log('d', d)
  sql.query(` UPDATE direcciones SET colonia = ?,nombre = ?,apellido = ?, estado = ? ,municipio = ?, calle=?,numero = ?,cp = ?,email = ?,telefono = ?
                WHERE iddireccion = ?`, [d.data.colonia, d.data.nombre ,d.data.apellido, d.data.estado,d.data.municipio,d.data.calle,d.data.numero,
                                         d.data.cp,d.data.email,d.data.telefono, d.data.iddireccion],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("actualiza direcciones: ", { id: d });
      result(null, { id: d });
    }
  );
};
Direcciones.cambiar_direccion_envio_activa = (d, result) => {
  sql.query(`SELECT iddireccion FROM direcciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
		if (err) { result(null, err);  return; }
    for(i in res){
      sql.query(` UPDATE direcciones SET activo = 0 WHERE iddireccion = ?`, [res[i].iddireccion]);
    }
    sql.query(`UPDATE direcciones SET activo = 1 WHERE iddireccion = ?`, [d.data.iddireccion] );
    result(null, true);
	})
};
Direcciones.eliminar_direccion_envio = (iddireccion, result) => {
  sql.query("DELETE FROM direcciones where iddireccion = ?;",[iddireccion], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`eliminado ${res.affectedRows} lista`);
    result(null, res);
  });
};

Direcciones.eliminar_direccion_facturacion = (idfacturacion, result) => {
  sql.query("DELETE FROM facturaciones where idfacturacion = ?;",[idfacturacion], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`eliminado ${res.affectedRows} lista`);
    result(null, res);
  });
};




Direcciones.direcciones_cliente_facturacion = (id, result)=>{
	sql.query(`SELECT d.* FROM facturaciones d WHERE idcliente=? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direcciones Facturacion: ", res);
    result(null, res);
	})
};
Direcciones.agregar_direccion_cliente_facturacion = (d, result) => {
	sql.query(`INSERT INTO facturaciones(idcliente,colonia,nombre,apellido, estado, municipio,calle,numero,cp,email,telefono,activo)
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,[d.idcliente ,d.data.colonia ,d.data.nombre,d.data.apellido, d.data.estado,d.data.municipio,
                                d.data.calle,d.data.numero,d.data.cp,d.data.email,d.data.telefono, d.activo], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    console.log("Crear Direcciones: ", { id: res.insertId, ...d });
    result(null, { id: res.insertId, ...d });
	});
};
Direcciones.actualiza_direccion_facturacion = (d, result) => {
  console.log('d', d)
  sql.query(` UPDATE facturaciones SET colonia = ?,nombre = ?,apellido = ?, estado = ?,municipio = ?, calle=?,numero = ?,cp = ?,email = ?,telefono = ?
                WHERE idfacturacion = ?`, [d.data.colonia, d.data.nombre ,d.data.apellido, d.data.estado, d.data.municipio,d.data.calle,d.data.numero,
                                           d.data.cp,d.data.email,d.data.telefono, d.data.idfacturacion],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("actualiza direcciones facturacion: ", { id: d });
      result(null, { id: d });
    }
  );
};
Direcciones.cambiar_direccion_facturacion_activa = (d, result) => {
  sql.query(`SELECT idfacturacion FROM facturaciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
		if (err) { result(null, err);  return; }
    for(i in res){
      sql.query(` UPDATE facturaciones SET activo = 0 WHERE idfacturacion = ?`, [res[i].idfacturacion]);
    }
    sql.query(`UPDATE facturaciones SET activo = 1 WHERE idfacturacion = ?`, [d.data.idfacturacion] );
    result(null, true);
	})
};


Direcciones.direccion_envio_activa = (id, result)=>{
	sql.query(`SELECT d.* FROM direcciones d WHERE idcliente=? AND activo = 1`, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direcciones Envio: ", res);
    result(null, res);
	})
};
Direcciones.direccion_facturacion_activa = (id, result)=>{
	sql.query(`SELECT f.* FROM facturaciones f WHERE idcliente=? AND activo = 1`, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direcciones Facturacion: ", res);
    result(null, res);
	})
};
Direcciones.agregar_direccion_cliente_activa = (d, result) => {
	sql.query(`INSERT INTO direcciones(idcliente,nombre,apellido,cp, colonia, estado, municipio, calle, numero, email,telefono)
		VALUES(?,?,?,?,?,?,?,?,?,?,?)`,[d.idcliente,d.data.nombre,d.data.apellido, d.data.cp, d.data.colonia, d.data.estado, d.data.municipio,
                              d.data.calle, d.data.numero,d.data.email,d.data.telefono], (err, response) => {	
    if (err) { result(err, null); return; };
    sql.query(`SELECT iddireccion FROM direcciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
      if (err) { result(null, err);  return; }
      for(i in res){
        sql.query(` UPDATE direcciones SET activo = 0 WHERE iddireccion = ?`, [res[i].iddireccion]);
      }
      result(null, { id: response.insertId, ...d });
    });
	});
};
Direcciones.activa_envio = (id, result) => {
  console.log('activa_envio', id)
  sql.query(`UPDATE direcciones SET activo = 1 WHERE iddireccion = ?`, [id],  (err,res)=>{
		if (err) { result(null, err);  return; }
    result(null, true);
	})
};


Direcciones.agregar_direccion_facturacion_cliente_activa = (d, result) => {
	sql.query(`INSERT INTO facturaciones(idcliente,nombre,apellido,cp, colonia, estado, municipio, calle, numero, email,telefono)
		VALUES(?,?,?,?,?,?,?,?,?,?,?)`,[d.idcliente,d.data.nombre,d.data.apellido, d.data.cp, d.data.colonia, d.data.estado, d.data.municipio,
                                d.data.calle, d.data.numero,d.data.email,d.data.telefono], (err, response) => {	
    if (err) { result(err, null); return; };


    sql.query(`SELECT idfacturacion FROM facturaciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
      if (err) { result(err, null);  return; }
      for(i in res){
        sql.query(` UPDATE facturaciones SET activo = 0 WHERE idfacturacion = ?`, [res[i].idfacturacion]);
      }
      result(null, { id: response.insertId, ...d });
    });
	});
};
Direcciones.activa_facturacion = (id, result) => {
  sql.query(`UPDATE facturaciones SET activo = 1 WHERE idfacturacion = ?`, [id],  (err,res)=>{
		if (err) { result(null, err);  return; }
    result(null, true);
	})
};


Direcciones.obtener_direccion_envio_docum = (id, result)=>{
	sql.query(`SELECT * FROM pago_direcciones WHERE idpago_direccion =? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direccion: ", res);
    result(null, res);
	})
};
Direcciones.obtener_direccion_facturacion_docum = (id, result)=>{
	sql.query(`SELECT * FROM pago_facturacion WHERE idpagofacturacion =? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Direccion: ", res);
    result(null, res);
	})
};






Direcciones.deleteDireccion = (id, result) => {
  sql.query("DELETE FROM direcciones WHERE iddirecciones = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted direcciones with id: ", id);
    result(null, res);
  });
};


module.exports = Direcciones;