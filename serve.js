// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors');
const fileUpload = require('express-fileupload')

// Para usar con certificado ssl
var http = require('http');
var https = require('https');
var fs = require('fs');


// IMPORTAR EXPRESS
const app = express();

// Rutas estaticas
app.use('/imagenes-bluevet', express.static('./../../imagenes-bluevet'));
app.use('/pdf_bluevet',      express.static('./../../pdf_bluevet'));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
require('./routes/users.routes')(app);
require('./routes/articulos.routes')(app);
require('./routes/laboratorios.routes')(app);
require('./routes/clientes.routes')(app);
require('./routes/correos.routes')(app);
require('./routes/entradas.routes')(app);
require('./routes/almacen.routes')(app);
require('./routes/salidas.routes')(app);
require('./routes/existencias.routes')(app);
require('./routes/documentos.routes')(app);
require('./routes/banners.routes')(app);
require('./routes/configuracion.routes')(app);
require('./routes/paypal.routes')(app);
require('./routes/direcciones.routes')(app);
require('./routes/datosfiscales.routes')(app);
require('./routes/ordenes.routes')(app);
require('./routes/pagos.routes')(app);
require('./routes/depositos.routes')(app);
require('./routes/facturas.routes')(app);

require('./routes/ciudades.routes')(app);
require('./routes/deseos.routes')(app);

require('./routes/envios.routes')(app);
require('./routes/mercadopago.routes')(app);

require('./routes/cupones.routes')(app);

require('./routes/sucursales.routes')(app);
require('./routes/caja/caja.routes')(app);
require('./routes/catalogos.routes')(app);


// ----FIN-DE-LAS-RUTAS-------------------------------------->

app.listen(3006, () => {
  console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
  console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
  console.log("|**************************************************************| ");
  console.log("|************ Servidor Corriendo en el Puerto 3006 ************| ");
});

