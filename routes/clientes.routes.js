module.exports = app => {
  const clientes = require('../controllers/clientes.controllers') // --> ADDED THIS

  app.get("/clientes.list"        , clientes.getClientes);   
  app.post("/clientes.add"        , clientes.addCliente);
  app.put("/clientes.update/:id"  , clientes.updateCliente);

  app.get("/mascotas.list"        , clientes.getMascotas);   
  app.post("/mascotas.add"        , clientes.addMascota);
  app.put("/mascotas.update/:id"  , clientes.updateMascota);

};