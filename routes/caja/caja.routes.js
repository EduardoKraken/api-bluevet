module.exports = app => {
  const caja = require('../../controllers/caja/caja.controllers') 
  app.get("/caja.existe/:id",        caja.exsiteCaja);
  app.post("/caja.abierta",          caja.abrirCaja);
  app.post("/caja.pago.add",         caja.addPagoCaja);

  // Detalle venta
  // Ventas generales
  app.get("/detalle_venta.list",     caja.getDetallesVenta);

  // Ruta para regresar cuanto dinero en efectivo debería tener la caja
  app.get("/caja.efectivo/:id",      caja.getEfectivoEnCaja);
  app.post("/caja.retiro.add",       caja.addRetiroCaja);
  app.post("/caja.cierre",           caja.addCierreCaja);

  app.post("/caja.cerradas",         caja.getCajasCerradas);
  app.get("/caja.cerradas/:id",      caja.getCajaCerrada);

  app.get('/caja.cierre/:id',        caja.getCierreCaja)

  // OBTENER LAS VENTAS DIARIAS
  app.post('/caja.ventas.diarias',   caja.getVentasDiarias)
};