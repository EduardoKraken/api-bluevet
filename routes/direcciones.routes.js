module.exports = app => {
  const direcciones = require('../controllers/direcciones.controllers') // --> ADDED THIS

  app.get("/direcciones.cliente/:idcliente"          , direcciones.direcciones_cliente);   
  app.post("/agregar.direccion.cliente"              , direcciones.agregar_direccion_cliente);  
  app.post("/actualiza.direccion.envio"              , direcciones.actualiza_direccion_envio);
  app.post("/cambiar.direccion.envio.activa"         , direcciones.cambiar_direccion_envio_activa);

  
  app.get("/direcciones.cliente.facturacion/:idcliente" , direcciones.direcciones_cliente_facturacion);   
  app.post("/agregar.direccion.cliente.facturacion"     , direcciones.agregar_direccion_cliente_facturacion);  
  app.post("/actualiza.direccion.facturacion"           , direcciones.actualiza_direccion_facturacion);
  app.post("/cambiar.direccion.facturacion.activa"      , direcciones.cambiar_direccion_facturacion_activa);

  app.get("/direccion.envio.activa/:idcliente"       , direcciones.direccion_envio_activa);   
  app.get("/direccion.facturacion.activa/:idcliente" , direcciones.direccion_facturacion_activa);   

  app.post("/agregar.direccion.cliente.activa"             , direcciones.agregar_direccion_cliente_activa); 
  app.post("/agregar.direccion.facturacion.cliente.activa" , direcciones.agregar_direccion_facturacion_cliente_activa);  

  app.delete("/eliminar.direccion.envio/:iddireccion", 	direcciones.eliminar_direccion_envio);
  app.delete("/eliminar.direccion.facturacion/:idfacturacion", 	direcciones.eliminar_direccion_facturacion);

  app.get("/obtener.direccion.envio.docum/:iddireccion" , direcciones.obtener_direccion_envio_docum);  
  app.get("/obtener.direccion.facturacion.docum/:idfacturacion" , direcciones.obtener_direccion_facturacion_docum);   

  
  
  
 


  // app.get("/direcciones.id/:id",         direcciones.getDirecciones);
  // app.delete("/direcciones.delete/:id",  direcciones.deleteDireccion);

};