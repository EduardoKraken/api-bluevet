module.exports = app => {
  const catalogos = require('../controllers/catalogos.controllers') // --> ADDED THIS

  app.get("/especies.list"     , catalogos.getEspecies);   
  app.get("/especies.activos"  , catalogos.getEspeciesActivas);   
  app.post("/especies.add"     , catalogos.addEspecie);
  app.put("/especies.update"   , catalogos.updateEspecie);

  app.get("/razas.list"        , catalogos.getRazas);   
  app.get("/razas.activos"     , catalogos.getRazasActivas);   
  app.post("/razas.add"        , catalogos.addRaza);
  app.put("/razas.update"      , catalogos.updateRaza);

};