module.exports = app => {
    const articulos = require('../controllers/articulos.controllers') // --> ADDED THIS

    app.get("/obtener.articulos.random", articulos.obtener_articulos_random);   // BUSCAR 


    // Apis generales
    app.get("/articulos.list"            , articulos.findAll);   // BUSCAR 
    app.post("/articulos.add"            , articulos.addArticulos);
    app.get("/articulos.codigo/:codigo"  , articulos.getArticuloCodigo);
    app.get("/articulos.fotos/:codigo"   , articulos.fotosxArt);
    app.post("/fotos.add"                , articulos.addFoto);
    app.get("/articulos.id/:id"          , articulos.getArticuloId);
    app.put("/articulos.update/:id"      , articulos.updateArticulos);
    app.get("/articulos.activos"         , articulos.getArticulosActivo);
    app.get("/articulos.caja/:id"        , articulos.getArticulosCaja);

    // Foto
    app.delete("/admin/foto.art.delete/:id",   articulos.deleteFoto);

    // Foto principal
    app.put("/update.foto.principal/:id", articulos.updateFotoPrincipal);




  };