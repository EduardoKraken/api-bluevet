module.exports = app => {
  const entradas = require('../controllers/entradas.controllers') // --> ADDED THIS

  // movil
  app.get("/entradas.list",       entradas.getEntradasAll);   // BUSCAR
  app.post("/entradas.fecha",     entradas.getEntradasFecha);   // BUSCAR
  app.post("/entradas.add",       entradas.addEntradas);
  app.put("/entradas.update/:id", entradas.updateEntrada);
  app.post("/entradas.eliminar",  entradas.eliminarEntrada);

};