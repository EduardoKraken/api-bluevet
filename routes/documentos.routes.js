module.exports = app => {
  const documentos = require('../controllers/documentos.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  // app.post("/imagenes",           documentos.addImagen);
  // app.post("/pdfs",               documentos.addPdf);
  // app.get("/download/:nom",       documentos.getDocumento);
  app.post("/add_imagenes/:id",      documentos.addImagenes);
  app.post("/pdfs",                  documentos.addPdf);

};